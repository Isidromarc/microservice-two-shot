import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import ShoeList from './ShoeList';
import ShoeForm from './ShoeForm';
import ShoeDetail from './ShoeDetail';
import Nav from './Nav';
import HatList from './HatList';
import HatForm from './HatForm';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes">
            <Route index element={ <ShoeList /> } />
            <Route path="new" element={ <ShoeForm /> } />
            <Route path=":id" element={ <ShoeDetail /> } />
          </Route>
          <Route path="hats">
            <Route index element={ <HatList /> } />
            <Route path="new" element={ <HatForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
