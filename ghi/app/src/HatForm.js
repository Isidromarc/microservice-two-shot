import {useEffect, useState} from 'react';

function HatForm(props) {
    const [locations, setLocations] = useState([])
    const [fabric, setFabric] = useState("")
    const [style_name, setStyle_Name] = useState("")
    const [color, setColor] = useState("")
    const [picture, setPicture] = useState("")
    const [location, setLocation] = useState("")

    const handleChange = setFunction => {
        return (
            function (event){
                setFunction(event.target.value)
            }
        )
    }

    const handleSubmit = async event => {
        event.preventDefault();
        const data = {}
        data.fabric = fabric
        data.style_name = style_name
        data.color = color
        data.picture_url = picture
        data.location = location
        console.log(data)
        const postURL = 'http://localhost:8090/api/hats/';
        const fetchOptions = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type":"application/json",
            }
        };

        const postResponse = await fetch(postURL, fetchOptions)
        if (postResponse.ok) {
            console.log("got it");
            const newPost = await postResponse.json();
            console.log(newPost);
            setFabric("");
            setStyle_Name("");
            setColor("");
            setPicture("");
            setLocation("");
        }
    }

    const fetchLocations = async () => {
        const locationsURL = "http://localhost:8100/api/locations/";
        const locationsResponse = await fetch(locationsURL);
        if (locationsResponse.ok){
            const data = await locationsResponse.json();
            setLocations(data.locations);
        }
    }

    useEffect(() => {
        fetchLocations();
    }, []);

    return (
        <div className="mt-3 card text-center mx-auto" style={{width:"400px"}}>
            <h1 className="card-header" style={{backgroundColor:"#b9f5f5"}}>Make a new Hat</h1>
            <form className="card-body" onSubmit={handleSubmit} id="create-hat-form">
                <div className="mb-3 form-floating">
                    <input className="form-control" onChange={handleChange(setFabric)} value={fabric} required type="text" id="fabric" placeholder="Fabric" />
                    <label htmlFor="fabric">Fabric</label>
                </div>
                <div className="mb-3 form-floating">
                    <input className="form-control" onChange={handleChange(setStyle_Name)} value={style_name} required type="text" id="style_name" placeholder="Style Name" />
                    <label htmlFor="style_name">Style</label>
                </div>
                <div className="mb-3 form-floating">
                    <input className="form-control" onChange={handleChange(setColor)} value={color} required type="text" id="color" placeholder="Color" />
                    <label htmlFor="color">Color</label>
                </div>
                <div className="mb-3 form-floating">
                    <input className="form-control" onChange={handleChange(setPicture)} value={picture} type="url" id="picture" placeholder="Picture URL" />
                    <label htmlFor="picture">Picture URL</label>
                </div>
                <select className="mb-3 form-control" onChange={handleChange(setLocation)} value={location} required id="location" name="location">
                    <option value="">Select a Location</option>
                    {locations.map(location => {
                        return (
                            <option value={location.href} key={location.href}>
                                {location.closet_name} / {location.shelf_number}
                            </option>
                        );
                    })}
                </select>
                <br/>
                <button className="btn btn-outline-info">Submit</button>
            </form>
        </div>
    )
}

export default HatForm;
