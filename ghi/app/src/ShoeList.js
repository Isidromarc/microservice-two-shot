import {useEffect, useState} from 'react';
import { NavLink } from 'react-router-dom';

function ShoeList(props) {
    const [shoes, setShoes] = useState([]);

    const fetchShoes = async () => {
        const shoeResponse = await fetch("http://localhost:8080/api/shoes/");
        if (shoeResponse.ok){
            const data = await shoeResponse.json();
            setShoes(data.shoes);
        } else {
            console.log(shoeResponse)
        }
    }

    const handleDelete = (id) => {
        return (
            async () => {
                const deleteShoeURL = 'http://localhost:8080/api/shoes/'
                const data = {
                    "id": id
                }
                const fetchOptions = {
                    method: "delete",
                    body: JSON.stringify(data),
                    headers: {
                        "Content-Type":"application/json"
                    }
                }

                const delResponse = await fetch(deleteShoeURL, fetchOptions);
                if (delResponse.ok){
                    console.log("did it")
                    setShoes(shoes.filter(shoe => shoe.id !== id ))
                }
            }
        )
    }

    const handleSortAlpha = (attribute) => {
        return (
            () => {
                const copy = [...shoes]
                copy.sort((shoe1, shoe2) => {
                    return (
                        (shoe1[attribute].toUpperCase() > shoe2[attribute].toUpperCase())? 1:
                        (shoe1[attribute].toUpperCase() < shoe2[attribute].toUpperCase())? -1: 0
                    )
                })
                setShoes(copy)
            }
        )
    }

    const handleSortBin = () => {
        const copy = [...shoes]
        copy.sort((a,b) => a.bin_number - b.bin_number)
        setShoes(copy)
    }

    useEffect(() => {
        fetchShoes();
    }, []);

    const noStyle = {
        background:"none",
        backgroundColor: "none",
        border: "none"
    }

    return (
        <>
            <div className="container mt-3 p-3 w-100 border d-flex flex-wrap" style={{backgroundColor:"#b9f5f5"}}>
                <div className="d-flex flex-grow-1 justify-content-center">
                    <img className="rounded p-auto text-center" style={{height: "200px"}} src="/NewShoeBanner.jpeg" alt="Image from Pexel" />
                </div>
                <div className="w-auto h-100 mt-auto mb-auto text-center flex-grow-1" style={{minWidth:"300px"}}>
                    <h3>Just bought a new shoe?</h3>
                    <NavLink className="btn btn-primary rounded-full" to="/shoes/new/">Add it to your wardrobe!</NavLink>
                </div>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>
                            <button style={noStyle} onClick={handleSortAlpha("manufacturer")}>Manufacturer</button>
                        </th>
                        <th>
                            <button style={noStyle} onClick={handleSortAlpha("model")}>Model</button>
                        </th>
                        <th>
                            <button style={noStyle} onClick={handleSortAlpha("color")}>Color</button>
                        </th>
                        <th>
                            <button style={noStyle} onClick={handleSortAlpha("closet")}>Closet</button>
                        </th>
                        <th>
                            <button style={noStyle} onClick={handleSortBin}>Bin #</button>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {shoes.map(shoe => {
                        return (
                            <tr key={shoe.href}>
                                <td> {shoe.manufacturer} </td>
                                <td> {shoe.model} </td>
                                <td> {shoe.color} </td>
                                <td> {shoe.closet} </td>
                                <td> {shoe.bin_number} </td>
                                <td> <NavLink to={`${shoe.id}/`}>Details</NavLink> </td>
                                <td> <button className="btn btn-outline-secondary" onClick={handleDelete(shoe.id)}> Delete </button> </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    )
}

export default ShoeList;
