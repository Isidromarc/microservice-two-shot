import {useEffect, useState} from 'react';
import { NavLink } from 'react-router-dom';

function HatList(props) {
    const [hats, setHats] = useState([]);

    const fetchHats = async () => {
        const hatsResponse = await fetch("http://localhost:8090/api/hats/");
        if (hatsResponse.ok){
            const data = await hatsResponse.json();
            setHats(data.hats);
        } else {
            console.log(hatsResponse)
        }
    }

    const handleDelete = (id) => {
        return (
            async () => {
                const deleteHatsURL = 'http://localhost:8090/api/hats/'
                const data = {
                    "id": id
                }
                const fetchOptions = {
                    method: "delete",
                    body: JSON.stringify(data),
                    headers: {
                        "Content-Type":"application/json"
                    }
                }

                const delResponse = await fetch(deleteHatsURL, fetchOptions);
                if (delResponse.ok){
                    console.log("did it")
                    setHats(hats.filter(hat => hat.id !== id ))
                }
            }
        )
    }

    useEffect(() => {
        fetchHats();
    }, []);

    const noStyle = {
        background:"none",
        backgroundColor: "none",
        border: "none"
    }

    return (
        <>
            <div className="container mt-3 p-3 w-100 border d-flex flex-wrap" style={{backgroundColor:"#b9f5f5"}}>
                <div className="d-flex flex-grow-1 justify-content-center">
                    <img className="rounded p-auto text-center" style={{height: "200px"}} src="/stock hats.jpg" alt="Image from Pexel" />
                </div>
                <div className="w-auto h-100 mt-auto mb-auto text-center flex-grow-1" style={{minWidth:"300px"}}>
                    <h3>Looking for your hat?</h3>
                    <NavLink className="btn btn-primary rounded-full" to="/hats/new/">Check your closet!</NavLink>
                </div>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>
                           Fabric
                        </th>
                        <th>
                            Style
                        </th>
                        <th>
                            Color
                        </th>
                        <th>
                            Image
                        </th>
                        <th>
                            Delete
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {hats.map(hat => {
                        return (
                            <tr key={hat.href}>
                                <td> {hat.fabric} </td>
                                <td> {hat.style_name} </td>
                                <td> {hat.color} </td>
                                <td> {hat.picture_url} </td>
                                <td> <button className="btn btn-outline-secondary" onClick={handleDelete(hat.id)}> Delete </button> </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    )
}

export default HatList;
