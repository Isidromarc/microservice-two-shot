import {useEffect, useState} from 'react';

function ShoeForm(props) {
    const [bins, setBins] = useState([])
    const [manufacturer, setManufacturer] = useState("")
    const [model, setModel] = useState("")
    const [color, setColor] = useState("")
    const [picture, setPicture] = useState("")
    const [bin, setBin] = useState("")

    const handleChange = setFunction => {
        return (
            function (event){
                setFunction(event.target.value)
            }
        )
    }

    const handleSubmit = async event => {
        event.preventDefault();
        const data = {}
        data.manufacturer = manufacturer
        data.model = model
        data.color = color
        data.picture = picture
        data.bin = bin

        const postURL = 'http://localhost:8080/api/shoes/';
        const fetchOptions = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type":"application/json",
            }
        };

        const postResponse = await fetch(postURL, fetchOptions)
        if (postResponse.ok) {
            console.log("got it");
            const newPost = await postResponse.json();
            console.log(newPost);
            setManufacturer("");
            setModel("");
            setColor("");
            setPicture("");
            setBin("");
        }
    }

    const fetchBins = async () => {
        const binsURL = "http://localhost:8100/api/bins/";
        const binsResponse = await fetch(binsURL);
        if (binsResponse.ok){
            const data = await binsResponse.json();
            setBins(data.bins);
        }
    }

    useEffect(() => {
        fetchBins();
    }, []);

    return (
        <div className="mt-3 card text-center mx-auto" style={{width:"400px"}}>
            <h1 className="card-header" style={{backgroundColor:"#b9f5f5"}}>Make a new Shoe</h1>
            <form className="card-body" onSubmit={handleSubmit} id="create-shoe-form">
                <div className="mb-3 form-floating">
                    <input className="form-control" onChange={handleChange(setManufacturer)} value={manufacturer} required type="text" id="manufacturer" placeholder="Manufacturer" />
                    <label htmlFor="manufacturer">Manufacturer</label>
                </div>
                <div className="mb-3 form-floating">
                    <input className="form-control" onChange={handleChange(setModel)} value={model} required type="text" id="model" placeholder="Model" />
                    <label htmlFor="model">Model</label>
                </div>
                <div className="mb-3 form-floating">
                    <input className="form-control" onChange={handleChange(setColor)} value={color} required type="text" id="color" placeholder="Color" />
                    <label htmlFor="color">Color</label>
                </div>
                <div className="mb-3 form-floating">
                    <input className="form-control" onChange={handleChange(setPicture)} value={picture} type="url" id="picture" placeholder="Picture URL" />
                    <label htmlFor="picture">Picture URL</label>
                </div>
                <select className="mb-3 form-control" onChange={handleChange(setBin)} value={bin} required id="bin" name="bin">
                    <option value="">Select a Bin</option>
                    {bins.map(bin => {
                        return (
                            <option value={bin.href} key={bin.href}>
                                {bin.closet_name} / {bin.bin_number}
                            </option>
                        );
                    })}
                </select>
                <br/>
                <button className="btn btn-outline-info">Submit</button>
            </form>
        </div>
    )
}

export default ShoeForm;
