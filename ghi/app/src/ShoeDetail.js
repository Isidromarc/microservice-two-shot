import {useEffect, useState} from 'react';
import { useParams } from 'react-router-dom';

function ShoeDetail(prop){
    const [shoe, setShoe] = useState({});
    const [manufacturer, setManufacturer] = useState("");
    const [model, setModel] = useState("");
    const [color, setColor] = useState("");
    const [closet, setCloset] = useState("");
    const [picture, setPicture] = useState("");
    const [bin, setBin] = useState("");

    const shoeID = useParams()
    console.log(shoeID.id)

    const fetchData = async () => {
        const shoeDetailURL = `http://localhost:8080/api/shoes/${shoeID.id}/`;
        console.log(shoeDetailURL)
        const shoeResponse = await fetch(shoeDetailURL)
        if (shoeResponse.ok){
            const shoe = await shoeResponse.json()
            setManufacturer(shoe.manufacturer);
            setModel(shoe.model);
            setColor(shoe.color);
            setCloset(shoe.bin.closet_name);
            setPicture(shoe.picture);
            setBin(shoe.bin.bin_number);
        }
    };

    useEffect(() => {
        fetchData();
    },[]);

    return (
        <div className="card">
            <img className="card-image" src={picture} alt="Picture goes here" />
            <div className="card-body">
                <p>manufacturer: {manufacturer}</p>
                <p>model: {model}</p>
                <p>color: {color}</p>
                <p>Closet: {closet}</p>
                <p>Bin: {bin}</p>
            </div>
        </div>
    )
}

export default ShoeDetail;
