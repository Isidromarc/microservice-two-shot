import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()

from shoes_rest.models import BinVO

def poll():
    while True:
        print('Shoes poller polling for data')
        try:
            response = requests.get('http://wardrobe-api:8000/api/bins/')
            data = json.loads(response.text)
            for bin in data["bins"]:
                defaults = {
                    "closet_name": bin["closet_name"],
                    "import_href": bin["href"],
                    "bin_number": bin["bin_number"]
                }

                BinVO.objects.update_or_create(
                    import_href = defaults["import_href"],
                    defaults = defaults
                )
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
