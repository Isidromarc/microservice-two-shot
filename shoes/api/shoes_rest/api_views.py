import json
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from shoes_rest.models import Shoe, BinVO
from common.json import ShoeListEncoder, ShoeDetailEncoder

@require_http_methods(["GET","POST","DELETE"])
def api_shoes (request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        content = {
            "shoes": shoes
        }

        return JsonResponse(content, encoder=ShoeListEncoder)

    elif request.method == "POST":
        shoe = json.loads(request.body)
        required_properties = [
            "manufacturer",
            "model",
            "color",
            "bin"
        ]

        for property in required_properties:
            try:
                shoe[property]
            except KeyError:
                return JsonResponse(
                    {
                        "Error": "Missing one or more required properties",
                        "Required properties": {
                            "manufacturer":"string",
                            "model":"string",
                            "color":"string",
                            "bin":"string"
                        }
                    }, status=400
                )

        try:
            bin_href = shoe["bin"]
            bin = BinVO.objects.get(import_href = bin_href)
            shoe["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse({"error":"Bin Does Not Exist"}, status=404)

        try:
            new_shoe = Shoe.objects.create(**shoe)
            return JsonResponse(new_shoe, encoder=ShoeDetailEncoder, safe=False)
        except TypeError:
            return JsonResponse(
                {
                    "Error": "Unexpected Property",
                    "Assignable Properties": {
                        "manufacturer":"string",
                        "model":"string",
                        "color":"string",
                        "picture":"string url",
                        "bin":"string href"
                    }
                }, status=400
            )

    else:
        shoe = json.loads(request.body)

        try:
            shoe_id = shoe["id"]
            count, _ = Shoe.objects.filter(id=shoe_id).delete()
            return JsonResponse({
                "Deleted": count > 0,
            })
        except KeyError:
            return JsonResponse(
                {
                    "error":"missing id property",
                    "id":"integer"
                }, status=400
            )

@require_http_methods(["GET"])
def api_shoe_detail (request, shoe_id=None):
    if request.method == "GET":
        try:
            shoe = Shoe.objects.get(id=shoe_id)
            return JsonResponse(shoe, encoder=ShoeDetailEncoder, safe=False)
        except Shoe.DoesNotExist:
            return JsonResponse(
                {
                    "error":"Shoe does not exist"
                }, status=404
            )
