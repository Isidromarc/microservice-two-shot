from django.db import models
from django.urls import reverse

class BinVO (models.Model):
    import_href = models.CharField(max_length=100, unique=True)
    closet_name = models.CharField(max_length=100)
    bin_number = models.SmallIntegerField()

class Shoe (models.Model):
    manufacturer = models.CharField(max_length = 60)
    model = models.CharField(max_length = 60)
    color = models.CharField(max_length = 20)
    picture = models.URLField(blank=True, null=True)
    bin = models.ForeignKey(
        "BinVO",
        related_name="shoes",
        on_delete=models.CASCADE
    )

    def get_api_url (self):
        return reverse("api_shoe_detail", kwargs={"shoe_id" : self.id})
