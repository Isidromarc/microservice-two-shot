import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()

from hats_rest.models import LocationVO
# from hats_rest.models import Something

def poll():
    while True:
        print('Hats poller polling for data')
        try:
            response = requests.get('http://wardrobe-api:8000/api/locations/')
            data = json.loads(response.text)
            print(data["locations"])
            for location in data["locations"]:
                defaults = {
                    "import_href": location["href"],
                    "closet_name": location["closet_name"],
                    "shelf_number": location["shelf_number"],
                    "section_number": location["section_number"]
                }
                LocationVO.objects.update_or_create(
                    import_href = defaults["import_href"],
                    defaults = defaults
                )
            
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
