from django.urls import path
from hats_rest.views import hat_list, hat_detail

urlpatterns = [
    path('hats/', hat_list, name ='hat-list'),
    path('hats/<int:hat_id>/', hat_detail, name ='hat_detail'),
]
