from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from hats_rest.models import Hat, LocationVO
from common.json import HatModelEncoder
import json





@require_http_methods(["GET", "POST", "DELETE"])
def hat_list(request):
    if request.method == 'GET':
        hats = Hat.objects.all()
        content = {
            "hats": hats
        }
        return JsonResponse(content, encoder=HatModelEncoder)
    elif request.method == "POST":
        hats = json.loads(request.body)
        required_properties = [
            "fabric",
            "style_name",
            "color",
            "picture_url",
            "location"
        ]
        for property in required_properties:
            try:
                hats[property]
            except KeyError:
                return JsonResponse(
                    {
                        "Error": "Missing one or more properties",
                        "Required properties": {
                            "fabric":"string",
                            "style_name":"string",
                            "color":"string",
                            "picture_url": "string",
                            "location":"string"

                        }
                    }, status=400
                )
        try:
            location_href = hats["location"]
            location = LocationVO.objects.get(import_href = location_href)
            hats["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse({"error":"Location Does Not Exist"}, status=404) 

        new_hat = Hat.objects.create(**hats)
        return JsonResponse(new_hat, encoder=HatModelEncoder, safe=False)

    else:
        hats = json.loads(request.body)

        try:
            hat_id = hats["id"]
            count, _ = Hat.objects.filter(id=hat_id).delete()
            return JsonResponse({
                "Deleted": count > 0,
            })  
        except KeyError:
            return JsonResponse(
                {
                    "error":"missing id property",
                    "id":"integer"
                }, status=400
            )    



@require_http_methods(["GET"])
def hat_detail (request, hat_id=None):
    if request.method == "GET":
        try:
            hat = Hat.objects.get(id=hat_id)
            return JsonResponse(hat, encoder=HatModelEncoder, safe=False)
        except Hat.DoesNotExist:
            return JsonResponse(
                {
                    "error":"Hat does not exist"
                }, status=404
            )
