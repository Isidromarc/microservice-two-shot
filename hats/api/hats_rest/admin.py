from django.contrib import admin
from hats_rest.models import Hat
# Register your models here.

admin.register(Hat)
class HatAdmin(admin.ModelAdmin):
    list_display = (
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
    )
