# Wardrobify

Team:

* Marce Isidro - Shoes
* Myron Atlas - Hats

## Design
We will bootstrap for the design of this project.

## Shoes microservice
Models: BinVO, Shoe

---BinVo Properities---
import_href - string, that points to the url of the original bin data from Wardrobe API
closet_name - string, name of the closet where the bin is located
bin_number - integer, the number of the bin

---Shoe Properties---
manufacturer - string, name of the shoe manufacturer
model - string, name of the model
color - string, color of the shoe
picture - URL string, url link to a picture of the shoe
bin - ForeignKey, referencing BinVO, related_name of "shoes" and onDelete models.CASCADE

port: 8080
host_name: localhost, shoes-api

"GET"    | api/shoes/          | get a list of shoes | required data: none
"POST"   | api/shoes/          | Create a new shoe   | required data: manufacturer, model, color, bin
"DELETE" | api/shoes/          | Delete a shoe       | required data: id
"GET"    | api/shoes/<int:id>/ | get shoe details    | required data: none

hats-api uses a polling service to wardrobe-api with a default wait time of 60s.
hats-api polls the Bins model of the wardrobe-api for data and uses it's href,
closet_name and bin_number to populate BinVO. Shoes-api requires that BinVO data be in
sync with Wardrobe Bin data for the microservice to function properly.

hats-api returns useful error messages when making some basic mistakes during api calling.

## Hats microservice
Models: Hat; has all the characterstics you would give a hat that you will stored alway in your wardrobe
        LocationVO; is set with properties set in case the user has a multi wardrobe home or storage areas for hats and/or other clothing items
I also applied functions that list what hat is stored where also a function to delete a hat incase it is sold,giving away, or destroyed. I also created a form for when the user goes shopping and adds a new hat to the collection.Also I allowed the user to upload a picture of their hat incase they want to start a picture catalog of their hat collection.
